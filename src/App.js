import { useState } from "react";
import "./styles.css";

/* 

Fetch the cuisines from the following url and create a grid/cards view
to display them.

Also, create an inputbox to filter the results and search for a specific cuisine

You can use third party libraries, like axios to get the data.

URL: https://run.mocky.io/v3/c7254590-8a48-452e-8d07-5a8b06dd70fb

*/

const endpoint = "https://run.mocky.io/v3/c7254590-8a48-452e-8d07-5a8b06dd70fb";

async function getFoods() {
  const res = await fetch(endpoint).then((x) => x.json());

  return res;
}

const foods = (await getFoods()) ?? [];

export default function App() {
  const [userFood, setUserFood] = useState("");

  const filteredFoods = foods.filter((food) => {
    if (userFood) {
      return food.value.toLowerCase().startsWith(userFood.toLowerCase());
    } else {
      return true;
    }
  });

  return (
    <div className="App">
      <form>
        <input
          type="text"
          name="food"
          placeholder="type a food"
          value={userFood}
          onChange={(evt) => setUserFood(evt.target.value)}
        />
      </form>

      <ul className="list">
        {filteredFoods.map((food) => {
          return <Item key={food.id} name={food.value} />;
        })}
      </ul>
    </div>
  );
}

function Item(props) {
  return (
    <li className="item">
      <article>
        <header>{props.name}</header>
      </article>
    </li>
  );
}
